function get_menu(session_name, session_selected=null) {
    let params = {
        'session_name': session_name,
        'session_selected': get_session(session_name, session_selected),
        'filter_by': get_filter_by(session_name)
    };

    if (session_selected) {
        localStorage.removeItem('level_name');
        localStorage.removeItem('sub_level_name');
    }
    else {
        let level_name = localStorage.getItem('level_name');
        let sub_level_name = localStorage.getItem('sub_level_name');
        if (level_name)
            params['level_name'] = level_name;
        if (sub_level_name)
            params['sub_level_name'] = sub_level_name;
    }

    _refresh_url(params, session_name);
    fetch(url_with_queries('menu', params)).then(response => {
        response.json().then(value => {
            search_menu_el.innerHTML = value['menu'];
            refresh_interface(value);
            c_menu_init(session_name);
        });
    });
}

function get_filter_by(session_name) {
    if (session_name != 'poe')
        return 0;
    let filter_el = document.querySelector('input[name="filter"]:checked');
    if (filter_el.id === 'edit-show-not-installed')
        return 1;
    if (filter_el.id === 'edit-show-both')
        return 2;
    return 0;
}
