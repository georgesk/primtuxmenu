const MAX_WIDTH = 48;
const MAX_HEIGHT = 48;

function preload(evt, selector, img_name, input_selector) {
    let files = evt.files;
    let _file = files[0];
    if (!_file)
        return;
    let reader = new FileReader();
    reader.onload = function(e) {
        resize_img(
            e.target.result,
            _file.type,
            selector,
            upload_file,
            img_name,
            input_selector
        );
    };
    reader.readAsDataURL(_file);
}

function resize_img(
    base64,
    img_type,
    selector,
    after_callback,
    img_name,
    input_selector
) {
    let img = document.createElement('img');
    img.src = base64;
    img.onload = function () {
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);

        let width = img.width;
        let height = img.height;

        if (width > height && width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
        }
        if (width < height && height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height;
            height = MAX_HEIGHT;
        }
        canvas.width = width;
        canvas.height = height;
        ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, width, height);

        let data_uri = canvas.toDataURL(img_type, 1);
        let preload_img = document.getElementById(selector);
        preload_img.classList.remove('hidden');

        if (img_type == 'image/svg+xml')
            preload_img.src = img.src;
        else
            preload_img.src = data_uri;

        let extension = img_type.substr(6);
        let plus_index = extension.indexOf('+');
        if (plus_index)
            extension = extension.substr(0, plus_index);
        let img_full_name = img_name + '.' + extension;
        after_callback(data_uri, img_full_name, input_selector);
    }
}

function DataURIToBlob(dataURI) {
    const splitDataURI = dataURI.split(',');
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1]);
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0];

    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i);

    return new Blob([ia], { type: mimeString });
}

function upload_file(dataurl, img_name, input_selector) {
    const data = new FormData();
    data.append('img', DataURIToBlob(dataurl));
    data.append('img_name', img_name);
    fetch(
        'upload-icon',
        {
            method: 'POST',
            body: data
        }
    ).then(response => {
        response.text().then(value => {
            document.getElementById(input_selector).value = img_name;
        });
    });
}
