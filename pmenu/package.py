import subprocess


def _pop(args):
    proc = subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )
    return proc.communicate()


def apt_exist(app_name):
    result = _pop([f'apt-cache search --names-only "^{app_name}$"'])
    if len(result[0]) > 1:
        return True
    return False


def all_apt_installed():
    _result = _pop(['apt list --installed'])

    raw_debs_installed = str(_result[0]).split(']')
    debs_installed = []

    for raw_line in raw_debs_installed:
        if raw_line.find('/') == -1:
            continue
        raw_deb = raw_line[raw_line.find('\\n') + 2:raw_line.find('/')]
        if not raw_deb:
            continue
        debs_installed.append(raw_deb)

    return set(debs_installed)


def apt_installed(app_name):
    result = _pop([f'dpkg -s "{app_name}"'])
    if len(result[0]) > 1:
        return True
    return False


def apt_name_for_desktop(desktop_path):
    result = _pop([f'dpkg -S "{desktop_path}"'])
    stdout = str(result[0])
    return stdout[0:stdout.find(':')]


def desktops_path_for_app(app_name):
    result = _pop([f'dpkg -L "{app_name}"'])
    list_of_files = str(result[0]).split('\\n')
    return [
        _file
        for _file in list_of_files
        if _file.endswith('.desktop')
    ]


def apt_install(app_name):
    proc = subprocess.Popen(
        ['apt-get install -f %s' % app_name],
        shell=True
    )
    result = proc.communicate()
    print(result)


def apt_remove(app_name):
    _pop(['apt-get remove -f %s' % app_name])


def give_all():
    result = _pop(
        ["dpkg --get-selections | sed 's:install$::'"]
    )[0].decode("utf-8").replace('\t', '').split('\n')
    return result
