from stringcolor import cs


def success(message):
    print(
        cs("Succès : ", "white", "green"),
        message
    )


def error(message, force_exit=True):
    print(
        cs("Erreur : ", "yellow", "red"),
        message
    )
    if force_exit:
        exit(1)


def warning(message):
    print(
        cs("Alerte : ", "white", "orange"),
        message
    )
