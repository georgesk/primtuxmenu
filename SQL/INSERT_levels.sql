/* session Jerry */

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) 
VALUES (
    'jerry',
    'Français',
    'french',
    'francais.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) 
VALUES (
    'jerry',
    'Mathématiques',
    'mathematics',
    'maths-cycle-1.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'jerry',
    'Activités artistiques',
    'artistic',
    'activites-artistiques.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'jerry',
    'Explorer le monde',
    'world',
    'explorer-le-monde.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'jerry',
    'Jeux',
    'games',
    'jeux.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'jerry',
    'Explorer le monde',
    "Le temps et l'espace",
    'world',
    'le-temps-et-l-espace.svg',
    0,
    0
);
INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'jerry',
    'Explorer le monde',
    'Sciences',
    'world',
    'sciences-cycle 1.svg',
    0,
    0
);

/* session Koda */

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Français',
    'french',
    'francais.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
 )
 VALUES (
    'koda',
    'Français',
    "Lecture-sons",
    'french',
    'lecture-sons.svg',
    0,
    0
 );

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Français',
    "Lecture-compréhension",
    'french',
    'lecture-comprehension.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Français',
    "Grammaire",
    'french',
    'grammaire.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'koda',
    'Français',
    "Conjugaison",
    'french',
    'conjugaison.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'koda',
    'Français',
    "Orthographe",
    'french',
    'orthographe.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'koda',
    'Français',
    "Lexique",
    'french',
    'lexique.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'koda',
    'Mathématiques',
    'mathematics',
    'maths-cycles-2-et-3.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Mathématiques',
    "Numération",
    'mathematics',
    'numeration.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Mathématiques',
    "Calcul",
    'mathematics',
    'calcul.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Mathématiques',
    "Grandeur et mesure",
    'mathematics',
    'grandeur-et-mesure.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Mathématiques',
    "Géométrie",
    'mathematics',
    'geometrie.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Langue vivante',
    'languages',
    'langue-vivante.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Histoire géographie',
    'world',
    'histoire-geographie.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Sciences',
    'sciences',
    'sciences.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Enseignements artistiques',
    'artistic',
    'activites-artistiques.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Enseignement moral et civique',
    'emc',
    'emc.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Jeux',
    'games',
    'jeux.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'koda',
    'Accessoires',
    'accessories',
    'accessoires.svg',
    0,
    0
);

/* session Léon */

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Français',
    'french',
    'francais.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Français',
    "Lecture-compréhension",
    'french',
    'lecture-comprehension.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Français',
    "Grammaire",
    'french',
    'grammaire.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Français',
    "Conjugaison",
    'french',
    'conjugaison.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Français',
    "Orthographe",
    'french',
    'orthographe.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Français',
    "Lexique",
    'french',
    'lexique.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Mathématiques',
    'mathematics',
    'maths-cycles-2-et-3.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Mathématiques',
    "Numération",
    'mathematics',
    'numeration.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Mathématiques',
    "Calcul",
    'mathematics',
    'calcul.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Mathématiques',
    "Grandeur et mesure",
    'mathematics',
    'grandeur-et-mesure.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Mathématiques',
    "Géométrie",
    'mathematics',
    'geometrie.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Langue vivante',
    'languages',
    'langue-vivante.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Histoire géographie',
    'world',
    'histoire-geographie.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Sciences',
    'sciences',
    'sciences.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'leon',
    'Enseignements artistiques',
    'artistic',
    'activites-artistiques.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Enseignement moral et civique',
    'emc',
    'emc.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'leon',
    'Jeux',
    'games',
    'jeux.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'leon',
    'Accessoires',
    'accessories',
    'accessoires.svg',
    0,
    0
);

/* session Poe */

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'poe',
    'Bureautique',
    'french',
    'bureautique.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'poe',
    'Multimédia',
    'world',
    'multimedia.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'poe',
    'Graphisme',
    'artistic',
    'graphisme.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'poe',
    'Outils du professeur',
    'games',
    'outils-du-professeur.svg',
    0,
    0
);
