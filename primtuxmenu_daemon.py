import sys

import pyinotify

from pmenu.conf import Conf
from pmenu.request import DataBase
from pmenu.requests.apps import DbApps
from pmenu.sync import sync_to_apt

import secrets

from flask import Flask
from flask_socketio import SocketIO, emit

app = Flask(__name__)

app.secret_key = secrets.token_urlsafe(32)
socketio = SocketIO(
    app,
    cors_allowed_origins="*",
    logger=True,
    engineio_logger=True
)


class LockedSingleton(object):
    is_locked = False

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(LockedSingleton, cls).__new__(cls)
        return cls.instance


class PrimtuxMenuEventHandler(pyinotify.ProcessEvent):
    def __init__(self, locked):
        conf = Conf()
        self.db = DataBase(conf)
        self.db_apps = DbApps(self.db)
        self.locked = locked

    def my_init(self, file_object=sys.stdout):
        pass

    def process_IN_CREATE(self, event):
        self._update_apps()

    def process_IN_CLOSE_WRITE(self, event):
        self._update_apps()

    def process_IN_DELETE(self, event):
        self._update_apps()

    def _update_apps(self):
        if self.locked.is_locked:
            return
        self.locked.is_locked = True
        sync_to_apt(self.db)
        self.locked.is_locked = False
        emit('watch')


@socketio.on('message')
def message(data):
    if 'first' not in data:
        return
    wm = pyinotify.WatchManager()
    notifier = pyinotify.Notifier(
        wm,
        PrimtuxMenuEventHandler(locked=LockedSingleton())
    )

    watch_files = [
        '/var/log/dpkg.log',
        '/var/log/apt/history.log',
    ]

    for d in watch_files:
        wm.add_watch(
            d,
            pyinotify.IN_CREATE
            | pyinotify.IN_DELETE
            | pyinotify.IN_CLOSE_WRITE
        )

    notifier.loop()


@socketio.on('connect')
def connect():
    emit('start')


if __name__ == "__main__":
    socketio.run(app, port=8000)
