# PrimtuxMenu

Le lanceur nouvelle génération de Primtux.
[Présentation](./PRESENTATION.md)

## Tester en local

1. Récupérer les sources git :

```sh
git clone git@framagit.org:mothsart/primtuxmenu.git && cd primtuxmenu
```

2. Activer les hooks git :

```sh
git config core.hooksPath=.githooks
```

3. Avoir un environnement virtuel

Il faut au préalable installer venv et pip :

```sh
sudo apt install python3.10-venv python3-pip
```

Puis :

```sh
python3 -m venv .
```

4. Installer les dépendances dans l'environnement virtuel

```sh
pip install -r requirements.txt
```

5. Lancer ce script :

```sh
make init
```

Il va dans l'ordre :

- Créer un espace virtuelle pour les dépendances python (évite d'installer des dépendances sur votre distribution).
- Installer les dépendances python pour un environnement de test et de dev.
- Alimentation de la Base de données.

## Récupération des derniers changements

```sh
make db.recover // git pull + make db.init
```

## Mettre à jour les paquets .deb avec une liste de fichiers provenant des sources du Primtuxmenu

1. On rajoute ou modifie le fichier .desktop dans le dossier desktops (qui les centralisent tous).

2. On rajoute ou modifie le fichier image (ex : .png) dans le dossier `static/assets/apps`.

3. On édite le fichier : debs_replace.txt
Pour chaque, on renseigne dans l'ordre :
* le .deb concerné
* le fichier a incorporer (image ou desktop)
* le chemin de destination

4. On lance la commande en renseignant les chemin du dossier contenant les `.deb` :

```sh
make path=<le chemin des .debs> deb.replace
```

## Publier des changements en Base de données

1. On ajoute les entrées SQL générés dans les sources :

```sh
make db.record
```

2. On crée un commit sur son poste avec ses modifs :

```sh
git commit -m "la description des changements"
```

3. On envoi son commit avec les modifs sur le serveur :

```sh
git push
```

## Démarrer le serveur en mode debug

```sh
make run.server
```

Si l'on souhaite tester l'installation de logiciels, 
il faut lancer en parallèle un daemon (ce dernier doit avoir les droits root afin de piloter **apt**) :

```sh
sudo make run.daemon
```

## Si vous avez tmux et tmuxp

```sh
make tmux
```

## Les tests

Afin de bénéficier des tests d'intégration, il vous faudra installer Cypress.
(cf https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements)

### Installer Cypress

1. avoir Npm :

```sh
sudo apt-get install npm
```

2. disposer de certaines dépendances

```sh
sudo apt install libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb
```

3. installer l'applicatif localement dans un cadre de développement

```sh
npm install cypress --save-dev
```

### Ouvrir Cypress

```sh
make cypress.open
```

## Création du paquet Debian

### Prérequis

Installation des paquets manquants :

```sh
sudo apt install dpkg build-essential devscripts dh-python python3-distutils-extra debhelper python3-pygments flake8
```

## Compilation

**Attention : avant de créer un paquet debian, la BBD va être remise à jour !**
**Si vous avez édités des changements sans avoir effectué de "make db.record", vos données seront perdues !** 

```sh
make build.debian
```

```sh
sudo dpkg -i ../primtuxmenu_0.1_all.deb
```
